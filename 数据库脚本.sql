/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.5.36 : Database - luckwheel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`luckwheel` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `luckwheel`;

/*Table structure for table `luck_product` */

DROP TABLE IF EXISTS `luck_product`;

CREATE TABLE `luck_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '商品名',
  `weight` float DEFAULT '0' COMMENT '概率',
  `flag` int(11) DEFAULT '1' COMMENT '是否展示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `luck_product` */

insert  into `luck_product`(`id`,`name`,`weight`,`flag`) values (13,'iPhone11',5.5,1),(14,'宝马',3,1),(15,'奔驰',6,1),(16,'10万现金',10,1),(17,'耳机',20,1),(18,'笔记本',3,1);

/*Table structure for table `luck_user` */

DROP TABLE IF EXISTS `luck_user`;

CREATE TABLE `luck_user` (
  `id` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(255) NOT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `luck_num` int(11) DEFAULT '0' COMMENT '可抽奖次数',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `luck_user` */

insert  into `luck_user`(`id`,`uid`,`uname`,`province`,`city`,`address`,`tel`,`luck_num`) values (0,'12','42','42','54','52','52',5),(0,'34',NULL,NULL,NULL,NULL,NULL,0);

/*Table structure for table `luck_user_product` */

DROP TABLE IF EXISTS `luck_user_product`;

CREATE TABLE `luck_user_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `pid` int(11) DEFAULT '0',
  `pname` varchar(255) DEFAULT NULL COMMENT '商品名字',
  `exchange` int(11) DEFAULT '0' COMMENT '是否兑换 0未发货  1发货了',
  `ltime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2943 DEFAULT CHARSET=utf8;

/*Data for the table `luck_user_product` */

insert  into `luck_user_product`(`id`,`uid`,`pid`,`pname`,`exchange`,`ltime`) values (2938,'12',0,'宝马',0,'2020-04-23 15:44:37'),(2939,'12',0,'iPhone11',0,'2020-04-23 15:45:46'),(2940,'12',0,'耳机',0,'2020-04-23 15:45:55'),(2941,'12',0,'10万现金',0,'2020-04-23 15:46:13'),(2942,'12',0,'笔记本',0,'2020-04-23 15:46:21');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
